<?php

use App\User;
use App\Role; 
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::find(1);
        
        $user = new User();
        $user->name = 'Administrator';
        $user->email = 'mayra130428@gmail.com';
        $user->password = bcrypt('secret');
        $user->role()->associate($role);
        $user->save();
    }
}
