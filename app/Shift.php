<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shift extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'shift', 'status',
    ];

    protected $dates = ['deleted_at'];

    public function movies()
    {
        return $this->hasMany(MovieShift::class);
    }
}
