<?php

namespace App\Http\Controllers\API;

use Validator;
use App\Shift;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ShiftController extends Controller
{
    private $attributes = ['shift', 'status'];

    private $messages = [
        'shift.required' => 'El turno es requerido.',
        'status.required' => 'El status es requerido.'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Shift::all()->toArray();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'shift' => 'required'
        ], $this->messages);

        $data['shift'] = $request->shift;
        if ($request->has('status')){
            $data['status'] = true;
        }else{
            $data['status'] = false;
        }
        $shift = new Shift($data);
        $shift->save();
        return $shift;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Shift $shift)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shift $shift)
    {
        $validatedData = $request->validate([
            'shift' => 'required'
        ], $this->messages);

        $shift->shift = $request['shift'];
        if ($request->has('status')){
            $shift->status = true;
        }else{
            $shift->status = false;
        }
        $shift->save();
        return $shift;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shift $shift)
    {
        $shift->delete();
        return Response()->json(array('message'   =>  'Turno eliminada con éxito'), 200);
    }
}
