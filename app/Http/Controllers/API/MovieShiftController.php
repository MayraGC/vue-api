<?php

namespace App\Http\Controllers\API;

use App\Movie;
Use App\Shift;
use App\MovieShift;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MovieShiftController extends Controller
{
    private $messages = [
        'shift.required' => 'El turno es requerido.',
        'movie.required' => 'La película es requerido.'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return MovieShift::with('movie')->with('shift')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ids = []; 
        $validatedData = $request->validate([
            'shift' => 'required',
            'movie' => 'required'
        ], $this->messages);

        $movie = Movie::find($request->movie);
        $shifts = json_decode($request->shift, true);
        foreach ($shifts as $shift_id) {
            $shift = Shift::find($shift_id);
            if(!MovieShift::where('movie_id', $movie->id)->where('shift_id', $shift->id)->first()){
                $movieShift = new MovieShift();
                $movieShift->movie()->associate($movie);
                $movieShift->shift()->associate($shift);
                $movieShift->save();
                array_push($ids, $movieShift->id); 
            }
        }
        return MovieShift::with('movie')->with('shift')->find($ids);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MovieShift  $movieShift
     * @return \Illuminate\Http\Response
     */
    public function show(MovieShift $movieShift)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MovieShift  $movieShift
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MovieShift $movieShift)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MovieShift  $movieShift
     * @return \Illuminate\Http\Response
     */
    public function destroy(MovieShift $movieShift)
    {
        $id = $movieShift->id;
        $movieShift->delete();
        return $id;
    }
}
