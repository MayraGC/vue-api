<?php

namespace App\Http\Controllers\API;

use Validator;
use App\Movie;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class MovieController extends Controller
{
    private $attributes = ['name', 'image', 'status'];

    private $messages = [
        'name.required' => 'El nombre es requerido.',
        'name.unique' => 'El nombre ya esta registrado en otra película.',
        'published_date.required' => 'La fecha de publicación es requerida.',
        'image.required' => 'La imagen es requerida.',
        'image.mimes' => 'Las extensiones de la imagen no son validas.'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Movie::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //make validation
        $validatedData = $request->validate([
            'name' => [
                'required',
                Rule::unique('movies')->where(function ($query) use($request) {
                    return $query->where('name', $request->name);
                }),
            ],
            'image' => 'required | mimes:jpg,png',
            'published_date' => 'required | date'
        ], $this->messages);


        $data['name'] = $request->name;
        $data['image'] = Storage::url($request->file('image')->storeAs('public/movies', $request->name . "." . $request['image']->getClientOriginalExtension()));
        $data['published_date'] = $request->published_date;
        if ($request->has('status')){
            $data['status'] = true;
        }else{
            $data['status'] = false;
        }

        $movie = new Movie($data);
        $movie->save();

        return $movie;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Movie $movie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movie $movie)
    {
        //make validation
        $input = array('name' => $request->name, 'image' => $request->image, 'published_date' => $request->published_date, 'status' => $request->status);

        $validator = Validator::make($input,[
            'name' => [
                'required',
                Rule::unique('movies')->ignore($movie->id)->where(function ($query) use($input) {
                    return $query->where('name', $input['name']);
                }),
            ],
            'published_date' => 'required | date'
        ],$this->messages);

        if($validator->fails()){
            $errorString = implode(",",$validator->errors()->all());
            return Response()->json($errorString, 422);
        }else{
            if ($request->hasFile('image')) {
                $filename = str_replace("/storage/","public/",$movie->image);
                Storage::delete($filename);
                $movie->image = Storage::url($request->file('image')->storeAs('public/movies', $request->name . "." . $request['image']->getClientOriginalExtension()));
            }
            
            $movie->name = $request['name'];
            $movie->published_date = $request['published_date'];

            if ($request->has('status')){
                $movie->status = true;
            }else{
                $movie->status = false;
            }

            $movie->save();
            return Response()->json(array('message'   =>  'Registro con éxito'), 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie)
    {
        $movie->delete();
        return Response()->json(array('message'   =>  'Película eliminada con éxito'), 200);
    }
}
