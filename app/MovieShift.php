<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovieShift extends Model
{
    public function movie()
    {
        return $this->belongsTo(Movie::class);
    }

    public function shift()
    {
        return $this->belongsTo(Shift::class);
    }
}
