<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Movie extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'image', 'published_date', 'status',
    ];

    protected $dates = ['deleted_at'];

    public function shifts()
    {
        return $this->hasMany(MovieShift::class);
    }
}
